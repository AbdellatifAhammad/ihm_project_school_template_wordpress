<!DOCTYPE html>
   <html <?php language_attributes();?> >
   <head>
   
       <meta charset="<?php bloginfo('charset') ?>">
       <title><?php bloginfo('name')?></title>
       <link rel="pingback" href="<?php bloginfo('pingback_url');?>"/>
       
       <?php wp_head();?>
  </head>
  <body <?php body_class('estsb-body')?>>
<section id="estsb-nav ">
<nav class="navbar navbar-expand-sm estsb-nav navbar-dark fixed-top">
  <a class="navbar-brand" href="/wordpress"><?php bloginfo('name')?></a>
  <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
      aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavId">
    <?php estsb_add_nav_menu()?>  
  </div>
</nav>
</section>


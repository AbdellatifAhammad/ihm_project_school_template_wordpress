<?php $unique_id = esc_attr( uniqid('search-form-')); ?>
<form  method="get" class="search-form" action="<?php echo esc_url(home_url('/')) ?>">
<div class="input-group estsb-searchform-div">
  <input class="form-control estsb-searchform-input" value="<?php the_search_query(  ); ?>" name="s"  id="<?php echo $unique_id;?>" type="search"  placeholder="search" aria-label="Recipient's " aria-describedby="my-addon">
  <span class="input-group-btn">
    <button type="submit" class="btn  estsb-searchform-btn"><i class="fa fa-search" aria-hidden="true"></i> </button>
  </span>
</div>
</form>

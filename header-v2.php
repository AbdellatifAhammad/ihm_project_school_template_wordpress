<!DOCTYPE html>
   <html <?php language_attributes();?> >
   <head>

       <meta charset="<?php bloginfo('charset') ?>">
       <meta name="viewport" content="">
       <title><?php bloginfo('name')?></title>
       <link rel="pingback" href="<?php bloginfo('pingback_url');?>"/>

       <?php wp_head();?>
  </head>
  <body <?php body_class('estsb-body')?>>
<section id="estsb-nav">
<nav class="navbar navbar-expand-sm estsb-nav navbar-dark fixed-top">
  <a class="navbar-brand" href="/wordpress"><?php bloginfo('name')?></a>
  <div class="ml-auto">
      <i class="fa fa-facebook-official ml-1" aria-hidden="true"></i>
      <i class="fa fa-twitter ml-1" aria-hidden="true"></i>
      <i class="fa fa-instagram ml-1" aria-hidden="true"></i>
      <i class="fa fa-linkedin ml-1" aria-hidden="true"></i>
  </div>
</nav>
</section>

<section class="estsb-header">
  <div class="container ">
    <div class="row mt-5">
      <div class="col-md-4 mt-4 estsb-img-container ">
      <img src="<?php echo get_stylesheet_directory_uri() . '/img/logo.jpg'; ?> " class="estsb-image-header">
      </div>
        <div class="col-md-4 mt-4">
        <h1 class=" estsb-text">EST SB</h1>
        <h5 class="estsb-text-2" dir="rtl">المدرسة العليا للتكنولوجيا سيدي بنور</h5>
        <h5 class="estsb-text-2">école superieure de tecnologie sidi bennoure</h5>
        </div>
        <div class="col-md-4 mt-4 estsb-img-container">
        <img src="<?php echo get_stylesheet_directory_uri() . '/img/ucd.jpg'; ?> " class="estsb-image-header ">
        </div>
    </div>
  </div>
  <div class="estsb-navbar">
  <nav class="navbar navbar-expand-sm estsb-navbar">
  <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
      aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavId">
    <div class="container color-white">
       <!-- <?php estsb_add_nav_menu()?>   -->
    </div>

  </div>
</nav>
  </div>
</section>

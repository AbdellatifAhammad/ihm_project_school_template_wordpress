<?php


include(get_theme_file_path('/includes/enqueue.php'));
include(get_theme_file_path('/includes/class-wp-bootstrap-navwalker.php'));
include(get_theme_file_path('/includes/menu.php'));
include(get_theme_file_path('/includes/nav-walker.php'));
include(get_theme_file_path('/includes/posts.php'));
include(get_theme_file_path('/includes/initializer.php'));
include(get_theme_file_path('/includes/widgets.php'));

add_action( 'wp_enqueue_scripts', 'estsb_add_styles' );
add_action( 'wp_enqueue_scripts', 'estsb_add_scripts' );
add_action('after_setup_theme','estsb_init');
add_action( 'init','estsb_add_custom_menu' );
add_action( "init",'estsb_custom_post_type' );
add_action( 'widgets_init','estsb_widgets');















<?php get_header('v2');?>
<section id="estsb-first-section-blog" class="estsb-first-section-blog">
  <div class="container">
 <div class="row">
   <div class="col-md-6">
      
   </div>
 </div>
 </div>
</section>
<!-- start the posts cards or feed here -->
<div class="container   mb-5">
    <div class="row">
    <div class="col-md-7">
      <div id="carouselId" class="carousel slide mt-5" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselId" data-slide-to="0" class="active"></li>
          <li data-target="#carouselId" data-slide-to="1"></li>
          <li data-target="#carouselId" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="estsb-events-slider" src="<?php echo get_stylesheet_directory_uri().'/img/slider.jpg'?>" alt="First slide">
            
          </div>
        <?php
    $args  =array(
      'post_type'=> 'events',

    );
    
    $eventposts = new WP_Query($args);
    
    if($eventposts->have_posts()){
while ($eventposts->have_posts()) {
  $eventposts->the_post();
?>
          <div class="carousel-item">
            <img class="estsb-events-slider" src="<?php echo the_post_thumbnail_url();?>"" alt="second slide">
            <div class="carousel-caption d-none d-md-block">
            <h3><?php the_title() ?></h3>
            <p><?php the_excerpt(  ) ?></p>
      </div>
          </div>
          <?php
         
};
    };
          ?>
        </div>
        <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    
    <?php
    $args  =array(
      'post_type'=> 'post'
    );
    $blogposts = new WP_Query($args);
while ($blogposts->have_posts()) {
  $blogposts->the_post();
  
?>
    <div class="estsb-posts-card mt-5 ">
    <div class="row">
      <?php if(has_post_thumbnail()){ ?>
      <div class="col-md-6 estsb-post-cover" >
      <img src="<?php echo get_the_post_thumbnail_url();?>" class=" estsb-post-img" style="background-size:cover" alt="">
      </div>
      <div class="col-md-6 mt-4 ">
      <h2 class="estsb-post-title"><?php the_title();?></h2>
      <article><?php the_excerpt()?></article>
      <p class="text-right estsb-post-writer" href="<?php echo get_author_posts_url( get_the_author_meta('ID')) ?>" ><?php the_author()?></p>
      <div class="read-more d-inline">
        <a name="" id="" class="btn btn-primary estsb-readmore-btn" href="<?php the_permalink();?>" role="button">Read more</a>
      <p class="text-right"><?php the_date() ?></p>
      
      </div>
      
      </div>
      <?php }else{ ?>
      <div class="col-md-12 mt-5  pl-5">
      <h2 class="estsb-post-title"><?php the_title();?></h2>
      <article><?php the_excerpt()?></article>
      <a class="text-right" href="<?php echo get_author_posts_url( get_the_author_meta('ID')) ?>"><?php the_author()?></a>
      <div class="read-more d-inline">
        <a name="" id="" class="btn btn-primary estsb-readmore-btn" href="<?php the_permalink();?>" role="button">Read more</a>
      <p class="text-right"><?php the_date() ?></p>
      </div>
      </div>
      <?php }?>
    </div>
    </div>
    
     
        <?php
}
?>
    </div>
    <div class="col-md-4  mt-5   mb-5 estsb-sidebar">
      <?php get_sidebar();?>
    </div>
    </div>
</div>
<!-- end of post card  -->


<section id="director_word" class="director_word">
<div class="container mb-5">
<div class="text-center pt-md-4">
            <h1>Mots du Directeur</h1>
            <div class="line "></div>
</div>
    <div class="row pt-3 ">
    <div class="col-md-5 mb-5">
            <div class="img-director mx-auto"></div>
            <br>
    </div>
    <div class="col-md-7">
    <p class="estsb-presentation"><i class="fa fa-quote-left" aria-hidden="true"></i> 
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, illo repellendus placeat voluptates sint inventore. Aliquid, tempore illum! Voluptates asperiores magnam doloribus distinctio voluptatum alias praesentium excepturi nihil voluptatibus laudantium?
            de 22 ans au plus au 31 Décembre de l’année du concours et doivent déposer leurs dossiers de pré-inscription avant le 30 Juin de chaque année universitaire.
            <i class="fa fa-quote-right" aria-hidden="true"></i>
        </p>
    </div>
    </div>
</div>
</section>


<section id="formation" class="estsb-third-section">
    <div class="container">
        <div class="text-center pt-md-4">
            <h1>Formation</h1>
            <div class="line "></div>
            <h4>DUT(diplome universitaire de techologie)</h4>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="header-card-img-1 mx-auto"></div>
                    <div class="card-body">
                        <h4 class="card-title">Genie Informatique</h4>
                        <p class="card-text">Filiere Genie informatique</p>
                        <a name="" id="" class="btn btn-outline-primary estsb-btn-download" href="#" role="button">Plus de detaille</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-center">
                    <div class="header-card-img-2 mx-auto"></div>
                    <div class="card-body">
                        <h4 class="card-title">Techique de managment</h4>
                        <p class="card-text">Filiere Techique de managment</p>
                        <a name="" id="" class="btn btn-outline-primary estsb-btn-download" href="<?php esc_url(home_url('test'));?>" role="button">Plus de detaille</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="header-card-img-3 mx-auto"></div>
                    <div class="card-body">
                        <h4 class="card-title">Genie Agronomique</h4>
                        <p class="card-text">Filiere Genie Agronomique</p>
                        <a name="" id="" class="btn btn-outline-primary estsb-btn-download" href="#" role="button">Plus de detaille</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer();?>
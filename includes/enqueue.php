<?php
function estsb_add_styles(){
  wp_enqueue_style( 'bootstrap-css',    get_template_directory_uri(  )."/css/bootstrap.min.css" );
  wp_enqueue_style( 'fontawesome-css',    get_template_directory_uri()."/css/font-awesome.min.css");
  wp_enqueue_style( 'main-css',    get_template_directory_uri()."/css/main.css",array(),rand(111,9999),'all' );
}
function estsb_add_scripts(){
     wp_deregister_script( 'jquery' );
     wp_register_script( 'jquery', includes_url('/js/jquery/jquery.js'), false, '', true );
     wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), false, true );
     wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array(), false, true );
     
}
?>
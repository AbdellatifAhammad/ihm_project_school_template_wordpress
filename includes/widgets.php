<?php
function estsb_widgets()
{
    register_sidebar([
      'name'=> __('ESTSB sidebar'),
      'id'  => 'estsb_sidebar',
      'description' => __('Sidebar for estsb school theme','esetsb'),
      'before_widget'=> '<div id="%1$s" class="widget estsb-sidebar-widget cleafix %2$s">',
      'after_widget'=>'</div>',
      'before_title'=>'<h4>',
      'after_title'=>'</h4>'
      
      ]);
}

?>
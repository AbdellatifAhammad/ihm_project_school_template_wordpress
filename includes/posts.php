<?php
///events post type for our school 
function estsb_custom_post_type(){
  register_post_type( 'events', array(
    'rewrite' =>array('slug' => 'events'),
    'labels'=>array(
      'name'=>'Events',
      'singular_name'=> 'Event',
      'add_new_item'=>'Add new Event',
      'edit_item'=>'Edit Project'
    ),
    'menu-icon' =>'dashicons-clipboard',
    'public'=>true,
    'has_archive'=>true,
    'supports'=>array(
        'title','thumbnail','editor','excerpt','comments'
    )
  )
 );

 register_post_type( 'filières', array(
  'rewrite' =>array('slug' => 'filieres'),
  'labels'=>array(
    'name'=>'Filières',
    'singular_name'=> 'Filière',
    'add_new_item'=>'Add new Filiere',
    'edit_item'=>'Edit Filiere'
  ),
  'menu-icon' =>'dashicons-clipboard',
  'public'=>true,
  'has_archive'=>true,
  'supports'=>array(
      'title','thumbnail','editor','excerpt','comments'
  )
)
);

}
?>
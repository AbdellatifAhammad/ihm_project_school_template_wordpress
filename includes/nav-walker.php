<?php
require('class-wp-bootstrap-navwalker.php');
function estsb_add_nav_menu(){
  wp_nav_menu(array(
    'theme_location'=>'bootstrap-menu',
    'menu_class'=>'navbar-nav mr-auto mt-2 mt-lg-0',
    'menu_id'=>'',
    'container_id'    => '',
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'depth'           => 2,
    'walker'          => new wp_bootstrap_navwalker(),

  ));
}

?>
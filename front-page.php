<?php get_header()?>
<!-- the first section start here -->
<section class="estsb-first-section" id="section1">
    <div class="intro-container">
        <h1 data-text="Ecole superiure de technologie" class="mb-4 pb-0">Ecole superiure de technologie <br> de Sidi Bennour </h1>
        <a href="#about" class="about-btn scrollto">Présentation</a>
    </div>
</section>
<section id="about" class="estsb-second-section">
    <div class="container text-center">
        <div class="text-center pt-md-4">
            <h1>Présentation</h1>
            <div class="line "></div>
        </div>
        <p class="estsb-presentation"><i class="fa fa-quote-left" aria-hidden="true"></i> L’Ecole Supérieure de Technologie Sidi Bennour (ESTSB) est un établissement public d’enseignement supérieur à finalité de formation des Techniciens Supérieurs. Elle a été créée en Août 2016 par
            le Ministère de l’Enseignement Supérieur, de la Formation des Cadres et de la Recherche Scientifique du Royaume du Maroc. L’ESTSB est une composante de l’Université Chouaib Doukkali d’El Jadida. Sa vocation est de former des Techniciens Supérieurs
            polyvalents, hautement qualifiés et immédiatement opérationnels après leur sortie de l’Ecole en tant que collaborateurs d’ingénieurs et de managers. Sont admis à l’ESTSB, les bacheliers de l’enseignement secondaire technique, scientifique
            et tertiaire. L’admission à l’Ecole (au de Diplôme Universitaire de Technologie « DUT ») se fait par voie de sélection par ordre de mérite après une présélection sur la base des notes obtenues au baccalauréat. Les candidats doivent être âgés
            de 22 ans au plus au 31 Décembre de l’année du concours et doivent déposer leurs dossiers de pré-inscription avant le 30 Juin de chaque année universitaire.
            <i class="fa fa-quote-right" aria-hidden="true"></i>
        </p>
    </div>
</section>
<section id="formation" class="estsb-third-section">
    <div class="container">
        <div class="text-center pt-md-4">
            <h1>Formation</h1>
            <div class="line "></div>
            <h4>DUT(diplome universitaire de techologie)</h4>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="header-card-img-1 mx-auto"></div>
                    <div class="card-body">
                        <h4 class="card-title">Genie Informatique</h4>
                        <p class="card-text">Filiere Genie informatique</p>
                        <a name="" id="" class="btn btn-outline-primary estsb-btn-download" href="#" role="button">Plus de detaille</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-center">
                    <div class="header-card-img-2 mx-auto"></div>
                    <div class="card-body">
                        <h4 class="card-title">Techique de managment</h4>
                        <p class="card-text">Filiere Techique de managment</p>
                        <a name="" id="" class="btn btn-outline-primary estsb-btn-download" href="<?php esc_url(home_url('test'));?>" role="button">Plus de detaille</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="header-card-img-3 mx-auto"></div>
                    <div class="card-body">
                        <h4 class="card-title">Genie Agronomique</h4>
                        <p class="card-text">Filiere Genie Agronomique</p>
                        <a name="" id="" class="btn btn-outline-primary estsb-btn-download" href="#" role="button">Plus de detaille</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="director_word" class="director_word">
<div class="container mb-5">
<div class="text-center pt-md-4">
            <h1>Mots du Directeur</h1>
            <div class="line "></div>
</div>
    <div class="row pt-3 ">
    <div class="col-md-5 mb-5">
            <div class="img-director mx-auto"></div>
            <br>
    </div>
    <div class="col-md-7">
    <p class="estsb-presentation"><i class="fa fa-quote-left" aria-hidden="true"></i> 
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, illo repellendus placeat voluptates sint inventore. Aliquid, tempore illum! Voluptates asperiores magnam doloribus distinctio voluptatum alias praesentium excepturi nihil voluptatibus laudantium?
            de 22 ans au plus au 31 Décembre de l’année du concours et doivent déposer leurs dossiers de pré-inscription avant le 30 Juin de chaque année universitaire.
            <i class="fa fa-quote-right" aria-hidden="true"></i>
        </p>
    </div>
    </div>
</div>
</section>

<?php get_footer();?>
<?php
get_header('v2'); ?>

    <div class="container">
        <div class="row">
            <?php
			while ( have_posts() ) :
				the_post();
				?>
                <div class="col-md-8  mt-5 single-post mb-5">
                  <div class="div col-md-12">
                   <img src="<?php echo the_post_thumbnail_url();?>" class="img-fluid" alt="">
                   <br><br><br>
                  </div>

                    <?php the_content()?>
                    <nav class="nav-single">
                        <h3 class="assistive-text">
                            <?php _e( 'Post navigation', 'twentytwelve' ); ?>
                        </h3>
                        <span class="btn btn-primary estsb-navigation-button"><?php previous_post_link()?></span>
                        <span class="btn btn-primary estsb-navigation-button"><?php next_post_link() ?></span>
                    </nav>
                </div>
                <div class="col-md-3  mt-5   mb-5 estsb-sidebar">
                    <?php get_sidebar();?>
                </div>

                <?php endwhile; // End of the loop. ?>
        </div>
    </div>

    <br><br>

    <?php get_footer(); ?>
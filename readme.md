# IHM project : Wordpress theme for our school

> in this project we try to make a theme like the themes that you can buy from themeforest or somewhere else ,totally from scratch .

##### the front-page

the first page when you visite the school web site so you can see a small introduction about the school , the filiers ,plus the director word

> section1
> ![](https://i.imgur.com/IAT6JIr.jpg)
> section2
> ![](https://i.imgur.com/t9QEG3T.jpg)
> section3
> ![](https://i.imgur.com/indlGcS.jpg)
> section4
> ![](https://i.imgur.com/bbCIbVf.png)

##### the index page or let's say the main page

in this page you can see the latest posts and also the school events (in a slider), plus a customized sidebar that you can created your self with the sidebar theme support that we offring you ,you can put calendar, latest posts,video ,images ...

![](https://i.imgur.com/oQ88Dok.png)

###### this the part of the posts in this page

![](https://i.imgur.com/H4dpx3e.png)

> **the generale look of the post ** > ![](https://i.imgur.com/lRvNhWG.png)

![](https://i.imgur.com/p31zXpa.png)

###### others page

the others page is just for single post and some other stuffs like that

![](https://i.imgur.com/8HSxrlU.png)

---

### the theme support that we have

##### for the post we have some theme spport things such as the menu support ,and the sidebar(widgets) support , the posts types support ,plus the usual posts support from the thumbnaill ,the excert ...

> **the widgets(sidebar components) and the menu support** > ![](https://i.imgur.com/Ok5SXwG.png)

> **the available widgets**
>
> ![](https://i.imgur.com/r1Q61Rk.png)

![](https://i.imgur.com/yVBVboW.png)

> **posts type**

![](https://i.imgur.com/exrVtNf.png)

> we hope that it's look good if you need any support or 'something to fix we are availble

---

**this job done by Abdellatif Ahammad & Azddine el harity**
